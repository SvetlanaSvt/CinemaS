CREATE DATABASE  IF NOT EXISTS `cinema_svt` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cinema_svt`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: cinema
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hall`
--

DROP TABLE IF EXISTS `hall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hall_name` varchar(45) NOT NULL,
  `row` int(11) DEFAULT NULL,
  `seat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hall`
--

LOCK TABLES `hall` WRITE;
/*!40000 ALTER TABLE `hall` DISABLE KEYS */;
INSERT INTO `hall` VALUES (1,'Синий',7,8),(2,'Зелёный',7,7),(3,'Красный',6,6);
/*!40000 ALTER TABLE `hall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(400) DEFAULT NULL,
  `rent_start` date DEFAULT NULL,
  `rent_end` date DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `genre` varchar(20) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'Алиса в зазеркалье','Новое захватывающее приключение Disney «Алиса в Зазеркалье» подарит зрителям долгожданную встречу с любимыми героями захватывающих рассказов, вышедших из-под пера Льюиса Кэрролла.','2016-06-09','2016-07-01',113,'фэнтези',5),(5,'Warcraft: Начало','У Королівстві Штормград, що розкинулося на родючих землях Азерота, ніхто не очікував, що одного разу відважним чоловікам Альянсу доведеться виступити проти ворога, з яким раніше стикатися не доводилося. Люди не змогли передбачити появу порталу, через який воїнам Штормграда протистоять... ','2016-06-12','2016-07-01',123,'фэнтези',5),(6,'Иллюзия обмана 2','Четыре Всадника возвращаются! Прошел год после запоминающегося противостояния с ФБР, однако это ничуть не умерило их пыл. Мерритт МакКинни (Вуди Харрельсон) и его друзья не перестают удивлять публику новыми номерами, но вскоре серьезной проблемой становится некий Уолтер (Дэниэл Рэдклифф).','2016-06-16','2016-07-16',129,'Боевик',5);
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin'),(2,'User');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rows`
--

DROP TABLE IF EXISTS `rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_number` int(11) NOT NULL,
  `seat_quantity` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_rows_hall1_idx` (`hall_id`),
  CONSTRAINT `fk_rows_hall1` FOREIGN KEY (`hall_id`) REFERENCES `hall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rows`
--

LOCK TABLES `rows` WRITE;
/*!40000 ALTER TABLE `rows` DISABLE KEYS */;
INSERT INTO `rows` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,1,8,1),(9,2,1,1),(10,2,2,1),(11,2,3,1),(12,2,4,1),(13,2,5,1),(14,2,6,1),(15,2,7,1),(16,2,8,1),(17,3,1,1),(18,3,2,1),(19,3,3,1),(20,3,4,1),(21,3,5,1),(22,3,6,1),(23,3,7,1),(24,3,8,1),(25,4,1,1),(26,4,2,1),(27,4,3,1),(28,4,4,1),(29,4,5,1),(30,4,6,1),(31,4,7,1),(32,4,8,1),(33,5,1,1),(34,5,2,1),(35,5,3,1),(36,5,4,1),(37,5,5,1),(38,5,6,1),(39,5,7,1),(40,5,8,1),(41,6,1,1),(42,6,2,1),(43,6,3,1),(44,6,4,1),(45,6,5,1),(46,6,6,1),(47,6,7,1),(48,6,8,1),(49,7,1,1),(50,7,2,1),(51,7,3,1),(52,7,4,1),(53,7,5,1),(54,7,6,1),(55,7,7,1),(56,7,8,1),(57,1,1,2),(58,1,2,2),(59,1,3,2),(60,1,4,2),(61,1,5,2),(62,1,6,2),(63,1,7,2),(64,2,1,2),(65,2,2,2),(66,2,3,2),(67,2,4,2),(68,2,5,2),(69,2,6,2),(70,2,7,2),(71,3,1,2),(72,3,2,2),(73,3,3,2),(74,3,4,2),(75,3,5,2),(76,3,6,2),(77,3,7,2),(78,4,1,2),(79,4,2,2),(80,4,3,2),(81,4,4,2),(82,4,5,2),(83,4,6,2),(84,4,7,2),(86,5,1,2),(87,5,2,2),(88,5,3,2),(89,5,4,2),(90,5,5,2),(91,5,6,2),(92,5,7,2),(93,6,1,2),(94,6,2,2),(95,6,3,2),(96,6,4,2),(97,6,5,2),(98,6,6,2),(99,6,7,2),(100,7,1,2),(101,7,2,2),(102,7,3,2),(103,7,4,2),(104,7,5,2),(105,7,6,2),(106,7,7,2),(107,1,1,3),(108,1,2,3),(109,1,3,3),(110,1,4,3),(111,1,5,3),(112,1,6,3),(113,2,1,3),(114,2,2,3),(115,2,3,3),(116,2,4,3),(117,2,5,3),(118,2,6,3),(119,3,1,3),(120,3,2,3),(121,3,3,3),(122,3,4,3),(123,3,5,3),(124,3,6,3),(125,4,1,3),(126,4,2,3),(127,4,3,3),(128,4,4,3),(129,4,5,3),(130,4,6,3),(131,5,1,3),(132,5,2,3),(133,5,3,3),(134,5,4,3),(135,5,5,3),(136,5,6,3),(137,6,1,3),(138,6,2,3),(139,6,3,3),(140,6,4,3),(141,6,5,3),(142,6,6,3);
/*!40000 ALTER TABLE `rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` time(6) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_session_hall1_idx` (`hall_id`),
  KEY `fk_session_movie1_idx` (`movie_id`),
  CONSTRAINT `fk_session_hall1` FOREIGN KEY (`hall_id`) REFERENCES `hall` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_session_movie1` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES (7,'12:00:00.000000',75.00,1,1),(8,'12:30:00.000000',70.00,2,5),(9,'12:30:00.000000',65.00,3,6),(10,'16:00:00.000000',75.00,1,1),(11,'21:00:00.000000',75.00,2,1),(12,'16:00:00.000000',65.00,3,5);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_number` int(11) NOT NULL,
  `seat_number` int(11) NOT NULL,
  `is_sold` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `rows_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_tickets_users1_idx` (`user_id`),
  KEY `fk_tickets_session1_idx` (`session_id`),
  KEY `fk_tickets_rows1_idx` (`rows_id`),
  CONSTRAINT `fk_tickets_rows1` FOREIGN KEY (`rows_id`) REFERENCES `rows` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_session1` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_users1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` VALUES (20,4,4,1,4,7,28),(21,4,5,1,4,7,29),(24,3,1,1,4,7,17),(25,3,2,1,4,7,18),(26,3,3,1,4,7,19),(30,2,4,1,4,7,12),(31,2,5,1,4,7,13),(32,3,4,1,4,7,20),(33,3,5,1,4,7,21),(38,3,6,1,4,7,22),(39,3,7,1,4,7,23),(40,2,6,1,4,7,14),(41,2,7,1,4,7,15),(44,5,4,1,4,7,36);
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `sex` varchar(45) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_users_role_idx` (`role_id`),
  CONSTRAINT `fk_users_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Svetik','svetik','Sveta','Svet','as3333@mail.ru','Женский','1982-01-01',1),(4,'admin','111',NULL,NULL,'as333333@mail.ru',NULL,'1990-05-01',1),(5,'MoviePro','111111','','','aa11111@mail.ru','Мужской','1998-05-16',2),(15,'Jeka','asdf','','','dsv@kjvjdfh','Мужской','1999-10-17',2),(18,'Ira','qqqqq','','','hdvb@hbvb','Мужской','1998-12-19',2),(19,'Katya','gvutc','','','fgxhj@jhvuv.jn','Женский','1998-11-18',2),(34,'ooo','ooo','','','oo@oo','Мужской','2000-11-19',2),(35,'uu','uu','','','uu@uu','Мужской','1999-11-19',2),(36,'kk','kk','','','kk@kk','Мужской','2001-09-17',2),(37,'ll','ll','','','ll@ll','Мужской','2000-10-18',2),(38,'aa','aa','','','aa@aa','Мужской','2002-11-18',2),(39,'pp','pp','','','pp@pp','Мужской','1999-09-17',2),(40,'ttt','ttt','','','tt@tt','Мужской','1999-10-16',2),(41,'gg','gg','','','ggg@ggg','Мужской','1999-11-18',2),(42,'hhjj','hhjj','','','hh@jj','Мужской','1997-12-19',2),(43,'nn','nn','','','hrbf@irb','Мужской','1998-12-19',2),(44,'mm','mm','','','fhf@ivb','Мужской','1999-12-18',2),(45,'xx','xx','','','xx@xx','Мужской','1998-12-19',2),(46,'dd','dd','','','dd@dd','Мужской','2000-12-18',2),(47,'cc','cc','','','cfedc@fvdf','Мужской','2000-11-18',2),(48,'xxx','xxx','','','xx@xx','Мужской','1999-10-18',2),(49,'ccc','ccc','','','cfedc@fvdf','Мужской','2001-11-19',2),(50,'vv','vv','','','vv@dd','Мужской','1998-11-18',2),(51,'cccc','cccc','','','cfedc@fvdf','Мужской','1999-11-19',2),(52,'bb','vb','','','vv@dd','Мужской','1998-12-19',2),(53,'hh','hh','','','hdvb@hbvb','Мужской','2000-11-18',2),(54,'ee','ee','','','eee@ee','Мужской','1998-10-18',2),(55,'hhh','hhh','','','hdvb@hbvb','Мужской','1998-11-18',2),(56,'tt','tt','','','tt@tt','Мужской','1999-12-18',2),(57,'ddd','ddd','','','da@da','Мужской','1999-11-18',2),(58,'ww','ww','','','eee@ee','Мужской','1998-10-18',2),(59,'jj','jj','','','jh@bib','Мужской','1999-12-19',2),(60,'ii','ii','','','ii@fg','Мужской','1999-11-19',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-30 17:29:00
