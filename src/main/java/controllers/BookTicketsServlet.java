package controllers;

import dao.DaoFactory;
import dto.SessionDTO;
import dto.TicketsDTO;
import dto.UserDTO;
import model.*;
import service.impl.SessionServiceImpl;
import service.impl.TicketsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svetik on 26.06.2016.
 *
 */
@WebServlet(name = "BookTicketsServlet", urlPatterns = "/pages/user/book-tickets")
public class BookTicketsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(Integer.valueOf(request.getParameter("sessionDTO")));
        Movie movie = sessionDTO.getMovie();
        Hall hall = sessionDTO.getHall();
        Session session = DaoFactory.getInstance().getSessionDao().getById(sessionDTO.getId());
        List<Rows> rowsList = DaoFactory.getInstance().getRowsDao().getListBy("hall_id", String.valueOf(hall.getId()));
        List<Rows> rowsListBooked = new ArrayList<>();
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        User user = DaoFactory.getInstance().getUsersDao().getById(userDTO.getId());
        for (Rows rows : rowsList) {
            String s = String.valueOf(rows.getRowNumber()) + rows.getSeatQuantity();
            String rs = request.getParameter(s);
            if (rs != null && rs.equals(s)){
                response.getWriter().println(rows.getId());
                rowsListBooked.add(rows);
            }
        }
        for (Rows rows : rowsListBooked) {
            TicketsDTO ticketsDTO = new TicketsDTO(rows.getRowNumber(),rows.getSeatQuantity(), true, user, session, rows);
            response.getWriter().println(ticketsDTO);
            TicketsServiceImpl.getInstance().save(ticketsDTO);
        }

        request.setAttribute("sessionDTO", sessionDTO);
        request.setAttribute("movie", movie);
        request.setAttribute("hall", hall);
        request.setAttribute("rowsListBooked", rowsListBooked);
        request.getRequestDispatcher("/pages/user/booked.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
