package controllers;

import dao.DaoFactory;
import dto.MovieDTO;
import dto.SessionDTO;
import dto.UserDTO;
import model.User;
import service.impl.MovieServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Svetik on 26.06.2016.
 *
 */
@WebServlet(name = "ToMainRegServlet", urlPatterns = "/main")
public class ToMainRegServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTOtemp = (UserDTO) request.getSession().getAttribute("user");
//        response.getWriter().println(userDTOtemp);
        User user = DaoFactory.getInstance().getUsersDao().getBy("login", userDTOtemp.getLogin());
//        response.getWriter().println(user);
        UserDTO userDTO = UserServiceImpl.getInstance().getById(user.getId());
        request.getSession().setAttribute("user", userDTO);
        request.getSession().removeAttribute("sessionDTO");
        List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
        request.setAttribute("movieDTOList", movieDTOList);
        response.sendRedirect("/");
//        request.getRequestDispatcher("movie.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
