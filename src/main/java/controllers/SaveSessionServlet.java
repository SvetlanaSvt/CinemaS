package controllers;

import dao.DaoFactory;
import dto.SessionDTO;
import model.Hall;
import model.Movie;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;
import java.time.LocalTime;

/**
 * Created by Svetik on 25.06.2016.
 *
 */
@WebServlet(name = "SaveSessionServlet", urlPatterns = "/pages/admin/saveSession")
public class SaveSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        int movieId = Integer.valueOf(request.getParameter("movieId"));
        Movie movie = DaoFactory.getInstance().getMovieDao().getById(movieId);
        String s = request.getParameter("time");
        String[] ss = s.split(":");
        int hour = Integer.valueOf(ss[0]);
        int minute = Integer.valueOf(ss[1]);

//        String s1 = s.concat(":00");
//        Time time = Time.valueOf(s1);
        LocalTime time = LocalTime.of(hour, minute);

//        response.getWriter().println(s);
//        response.getWriter().println(request.getParameter("hallName"));
//
//        response.getWriter().println(time);
        Double price = Double.valueOf(request.getParameter("price"));
        String hallName = request.getParameter("hallName");
        Hall hall = DaoFactory.getInstance().getHallDao().getBy("hall_name", hallName);
        SessionDTO sessionDTO = new SessionDTO(time, price, hall, movie);
//        response.getWriter().println(hall);
//        response.getWriter().println();
        SessionServiceImpl.getInstance().save(sessionDTO);
        request.getSession().setAttribute("message", "Сеанс добавлен в базу");
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
