package controllers;

import dto.MovieDTO;
import dto.SessionDTO;
import dto.UserDTO;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Svetik on 20.06.2016.
 *
 */
@WebServlet(name = "LoginServlet", urlPatterns={"/login"})
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getSession().getAttribute("sessionId") == null) {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            UserDTO userDTO = UserServiceImpl.getInstance().getByLogin(login);
            if(userDTO != null && userDTO.getPassword().equals(password)){
                List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
                request.getSession().setAttribute("movieDTOList", movieDTOList);
                request.getSession().setAttribute("user", userDTO);
                response.sendRedirect(request.getSession().getAttribute("url").toString());
            }else{
                request.getSession().setAttribute("message", "Неправильно введён логин или пароль");
                response.sendRedirect(request.getContextPath() + "/pages/common/login.jsp");
            }
        }else {
            int sessionId = Integer.valueOf(request.getParameter("sessionId"));
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            UserDTO userDTO = UserServiceImpl.getInstance().getByLogin(login);
            if (userDTO != null && userDTO.getPassword().equals(password)) {
                List<MovieDTO> movieDTOList = MovieServiceImpl.getInstance().getAll();
                SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(sessionId);
                request.getSession().setAttribute("sessionDTO", sessionDTO);
                request.getSession().setAttribute("movieDTOList", movieDTOList);
                request.getSession().setAttribute("user", userDTO);
                response.sendRedirect(request.getSession().getAttribute("url").toString());
            } else {
                request.getSession().setAttribute("message", "Неправильно введён логин или пароль");
                response.sendRedirect(request.getContextPath() + "/pages/common/login.jsp");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
