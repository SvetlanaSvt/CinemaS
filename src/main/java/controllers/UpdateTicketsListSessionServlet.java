package controllers;

import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Svetik on 29.06.2016.
 *
 */
@WebServlet(name = "UpdateTicketsListSessionServlet", urlPatterns = "/pages/admin/updateTicketsListSession")
public class UpdateTicketsListSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        int id = Integer.valueOf(request.getParameter("movieId"));
        MovieDTO movieDTO = MovieServiceImpl.getInstance().getById(id);
        request.setAttribute("movieDTO", movieDTO);
        List<SessionDTO> sessionDTOs = SessionServiceImpl.getInstance().getSessionByMovieId(id);
        request.setAttribute("sessionDTOs", sessionDTOs);
        request.getRequestDispatcher("/pages/admin/updateTicketsListSession.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
