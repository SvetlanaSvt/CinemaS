package controllers;

import dao.DaoFactory;
import model.Session;
import model.Tickets;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.TicketsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svetik on 05.07.2016.
 *
 */
@WebServlet(name = "DeleteMovieIdServlet", urlPatterns = "/pages/admin/deleteMovieId")
public class DeleteMovieIdServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        int id = Integer.valueOf(request.getParameter("id"));
        List<Session> sessions = DaoFactory.getInstance().getSessionDao().getListBy("movie_id", String.valueOf(id));
        for (Session session : sessions) {
            List<Tickets> tickets = DaoFactory.getInstance().getTicketsDao().getListBy("session_id", String.valueOf(session.getId()));
            for (Tickets ticket : tickets) {
                TicketsServiceImpl.getInstance().delete(ticket.getId());
            }
            SessionServiceImpl.getInstance().delete(session.getId());
        }
        MovieServiceImpl.getInstance().delete(id);
        response.getWriter().println(sessions);
        request.getSession().setAttribute("message", "Фильм удалён из базы");
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
