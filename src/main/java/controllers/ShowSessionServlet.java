package controllers;

import dao.DaoFactory;
import dto.HallDTO;
import dto.MovieDTO;
import dto.SessionDTO;
import dto.UserDTO;
import model.Hall;
import model.Movie;
import model.Rows;
import model.Tickets;
import service.impl.HallServiceImpl;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svetik on 25.06.2016.
 *
 */
@WebServlet(name = "ShowSessionServlet", urlPatterns = "/pages/user/session")
public class ShowSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        if(request.getSession().getAttribute("sessionDTO") != null) {
            SessionDTO sessionDTO = (SessionDTO) request.getSession().getAttribute("sessionDTO");
            request.setAttribute("sessionDTO", sessionDTO);
            Hall hall = sessionDTO.getHall();
            request.setAttribute("hall", hall);
            List<Rows> rowsList = DaoFactory.getInstance().getRowsDao().getListBy("hall_id", String.valueOf(hall.getId()));
            request.setAttribute("rowsList", rowsList);
            List<Integer> rowNumbers = new ArrayList<>();
            for (int i = 1; i <= hall.getRow(); i++) {
                rowNumbers.add(i);
            }
            request.setAttribute("rowNumbers", rowNumbers);
            Movie movie = sessionDTO.getMovie();
            request.setAttribute("movie", movie);
            List<Tickets> ticketsList = DaoFactory.getInstance().getTicketsDao().getListBy("session_id", String.valueOf(sessionDTO.getId()));
            request.setAttribute("ticketsList", ticketsList);

        }else {
            Integer sessionId = Integer.valueOf(request.getSession().getAttribute("sessionId").toString());
            SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(sessionId);
            request.setAttribute("sessionDTO", sessionDTO);
            Hall hall = sessionDTO.getHall();
            request.setAttribute("hall", hall);
            List<Rows> rowsList = DaoFactory.getInstance().getRowsDao().getListBy("hall_id", String.valueOf(hall.getId()));
            request.setAttribute("rowsList", rowsList);
            List<Integer> rowNumbers = new ArrayList<>();
            for (int i = 1; i <= hall.getRow(); i++) {
                rowNumbers.add(i);
            }
            request.setAttribute("rowNumbers", rowNumbers);
            Movie movie = sessionDTO.getMovie();
            request.setAttribute("movie", movie);
            List<Tickets> ticketsList = DaoFactory.getInstance().getTicketsDao().getListBy("session_id", String.valueOf(sessionDTO.getId()));
            request.setAttribute("ticketsList", ticketsList);

        }

//        Hall hall = sessionDTO.getHall();
//        request.setAttribute("hall", hall);
//        String s = String.valueOf(hall.getId());
//        response.getWriter().println(sessionDTO);
//        response.getWriter().println(request.getSession().getAttribute("sessionId"));
//        response.getWriter().println(request.getSession().getAttribute("sessionDTO"));

        request.getRequestDispatcher("/pages/user/hall.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
