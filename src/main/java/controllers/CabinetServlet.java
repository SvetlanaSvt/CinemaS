package controllers;

import dao.DaoFactory;
import dto.UserDTO;
import model.Tickets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Svetik on 05.07.2016.
 *
 */
@WebServlet(name = "CabinetServlet", urlPatterns = "/pages/user/cabinet")
public class CabinetServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute("user");
        List<Tickets> ticketsList = DaoFactory.getInstance().getTicketsDao().getListBy("user_id", String.valueOf(userDTO.getId()));
        request.setAttribute("ticketsList", ticketsList);
        request.getRequestDispatcher("cabinet.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
