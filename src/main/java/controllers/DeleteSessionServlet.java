package controllers;

import dao.DaoFactory;
import dto.SessionDTO;
import model.Tickets;
import service.impl.SessionServiceImpl;
import service.impl.TicketsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Svetik on 07.07.2016.
 *
 */
@WebServlet(name = "DeleteSessionServlet", urlPatterns = "/pages/admin/deleteSession")
public class DeleteSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        int id = Integer.valueOf(request.getParameter("id"));
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(id);
        List<Tickets> tickets = DaoFactory.getInstance().getTicketsDao().getListBy("session_id", String.valueOf(sessionDTO.getId()));
        for (Tickets ticket : tickets) {
            TicketsServiceImpl.getInstance().delete(ticket.getId());
        }
        SessionServiceImpl.getInstance().delete(id);
        request.getSession().setAttribute("message", "Сеанс удалён из базы");
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
