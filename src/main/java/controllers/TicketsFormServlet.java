package controllers;

import dao.DaoFactory;
import dto.SessionDTO;
import model.Hall;
import model.Movie;
import model.Rows;
import model.Tickets;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svetik on 05.07.2016.
 *
 */
@WebServlet(name = "TicketsFormServlet", urlPatterns = "/pages/admin/ticketsForm")
public class TicketsFormServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        int sessionId = Integer.valueOf(request.getParameter("sessionId"));
        List<Tickets> ticketsList = DaoFactory.getInstance().getTicketsDao().getListBy("session_id", request.getParameter("sessionId"));
        response.getWriter().println(ticketsList);
        request.setAttribute("ticketsList", ticketsList);
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(sessionId);
        request.setAttribute("sessionDTO", sessionDTO);
        Hall hall = sessionDTO.getHall();
        request.setAttribute("hall", hall);
        List<Rows> rowsList = DaoFactory.getInstance().getRowsDao().getListBy("hall_id", String.valueOf(hall.getId()));
        request.setAttribute("rowsList", rowsList);
        List<Integer> rowNumbers = new ArrayList<>();
        for (int i = 1; i <= hall.getRow(); i++) {
            rowNumbers.add(i);
        }
        request.setAttribute("rowNumbers", rowNumbers);
        Movie movie = sessionDTO.getMovie();
        request.setAttribute("movie", movie);
        request.getRequestDispatcher("/pages/admin/ticketsForm.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
