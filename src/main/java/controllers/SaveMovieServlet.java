package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by Svetik on 24.06.2016.
 *
 */
@WebServlet(name = "SaveMovieServlet", urlPatterns = "/pages/admin/update")
public class SaveMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        int duration = Integer.valueOf(request.getParameter("duration"));
        LocalDate rentStart = LocalDate.of(Integer.valueOf(request.getParameter("yearStart")), Integer.valueOf(request.getParameter("monthStart")), Integer.valueOf(request.getParameter("dayStart")));
        LocalDate rentEnd = LocalDate.of(Integer.valueOf(request.getParameter("yearEnd")), Integer.valueOf(request.getParameter("monthEnd")), Integer.valueOf(request.getParameter("dayEnd")));
        String genre = request.getParameter("genre");
        int rating = Integer.valueOf(request.getParameter("rating"));
        MovieDTO movieDTO = new MovieDTO(title, description, duration, rentStart, rentEnd, genre, rating);
        MovieServiceImpl.getInstance().save(movieDTO);
        request.setAttribute("movieDTO", movieDTO);
        request.getSession().setAttribute("message", "Фильм добавлен в базу");
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
