package controllers;

import dao.DaoFactory;
import dto.HallDTO;
import dto.SessionDTO;
import model.Rows;
import model.Tickets;
import service.impl.HallServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.TicketsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svetik on 05.07.2016.
 *
 */
@WebServlet(name = "UnBookTicketsServlet", urlPatterns = "/pages/admin/unBookTickets")
public class UnBookTicketsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(Integer.valueOf(request.getParameter("sessionDTO")));
//        response.getWriter().println(sessionDTO);
//        response.getWriter().println(request.getParameter("sessionDTO"));
        HallDTO hallDTO = HallServiceImpl.getInstance().getById(Integer.valueOf(request.getParameter("hall")));
//        response.getWriter().println(hallDTO);
        List<Tickets> tickets = DaoFactory.getInstance().getTicketsDao().getListBy("session_id", String.valueOf(sessionDTO.getId()));
        List<Rows> rowsList = DaoFactory.getInstance().getRowsDao().getListBy("hall_id", String.valueOf(hallDTO.getId()));
        List<Rows> rowsListUnBook = new ArrayList<>();
        for (Rows rows : rowsList) {
            String s = String.valueOf(rows.getRowNumber()) + rows.getSeatQuantity();
            String rs = request.getParameter(s);
            if (rs != null && rs.equals(s)){
                response.getWriter().println(rows.getId());
                rowsListUnBook.add(rows);
//                response.getWriter().println(rows);
            }
        }
        for (Rows rows : rowsListUnBook) {
            for (Tickets ticket : tickets) {
                if(rows.getId().equals(ticket.getRows().getId())) {
                    TicketsServiceImpl.getInstance().delete(ticket.getId());
                }
            }
        }
        request.getSession().setAttribute("message", "Вы сняли бронь выбранных билетов");
        response.sendRedirect(request.getContextPath() + "/pages/admin/admin.jsp");


//        TicketsServiceImpl.getInstance().delete(rows.getId());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
