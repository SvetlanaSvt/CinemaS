package controllers;

import dto.MovieDTO;
import dto.SessionDTO;
import service.impl.MovieServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Svetik on 07.07.2016.
 *
 */
@WebServlet(name = "DeleteSessionIdServlet", urlPatterns = "/pages/admin/deleteSessionIdMovie")
public class DeleteSessionIdMovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.valueOf(request.getParameter("id"));
        MovieDTO movieDTOs = MovieServiceImpl.getInstance().getById(id);
        request.setAttribute("movieDTOs", movieDTOs);
        List<SessionDTO> sessionDTOs = SessionServiceImpl.getInstance().getSessionByMovieId(id);
        request.setAttribute("sessionDTOs", sessionDTOs);
        request.getRequestDispatcher("deleteSession.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
