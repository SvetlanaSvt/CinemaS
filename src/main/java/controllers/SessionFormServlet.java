package controllers;

import dto.MovieDTO;
import service.impl.MovieServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Svetik on 25.06.2016.
 *
 */
@WebServlet(name = "SessionFormServlet", urlPatterns = "/pages/admin/sessionForm")
public class SessionFormServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.valueOf(request.getParameter("id"));
        MovieDTO movieDTOs = MovieServiceImpl.getInstance().getById(id);
        request.setAttribute("movieDTOs", movieDTOs);
        request.getRequestDispatcher("/pages/admin/sessionForm.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
