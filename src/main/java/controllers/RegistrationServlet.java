package controllers;

import dao.DaoFactory;
import dto.UserDTO;
import model.Role;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by Svetik on 20.06.2016.
 *
 */
@WebServlet(name = "RegistrationServlet", urlPatterns={"/registration"})
public class RegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String email = request.getParameter("email");
        String sex = request.getParameter("sex");
        LocalDate birthday = LocalDate.of(Integer.valueOf(request.getParameter("year")), Integer.valueOf(request.getParameter("month")), Integer.valueOf(request.getParameter("day")));
        Role role = DaoFactory.getInstance().getRoleDao().getBy("role_name", "User");
        UserDTO userDTO = new UserDTO(login, password, firstname, lastname, email, sex, birthday, role);
        int i = UserServiceImpl.getInstance().searchUser(userDTO);
        if (i == 0) {
            request.getSession().setAttribute("user", userDTO);
            UserServiceImpl.getInstance().save(userDTO);
            request.getSession().setAttribute("url", request.getRequestURI());
            request.getSession().setAttribute("message", "Регистрация прошла успешно");
            response.sendRedirect(request.getContextPath() + "/main");
        } else if(i == 1) {
            request.getSession().setAttribute("message", "Логин уже существует");
            response.sendRedirect(request.getContextPath() + "/pages/common/registration.jsp");
        }



}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
