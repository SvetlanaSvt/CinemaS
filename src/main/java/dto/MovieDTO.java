package dto;



import java.time.LocalDate;
import java.time.format.DateTimeFormatter;



/**
 * Created by Svetik on 08.06.2016.
 *
 */
public class MovieDTO extends Entity<Integer> {

    private String title;
    private String description;
    private int duration;
    private LocalDate rentStart;
    private LocalDate rentEnd;
    private String genre;
    private int rating;


    public MovieDTO() {
    }

    public MovieDTO(String title, String description, int duration, LocalDate rentStart, LocalDate rentEnd, String genre, int rating) {
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.rentStart = rentStart;
        this.rentEnd = rentEnd;
        this.genre = genre;
        this.rating = rating;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public LocalDate getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalDate rentStart) {
        this.rentStart = rentStart;
    }

    public LocalDate getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(LocalDate rentEnd) {
        this.rentEnd = rentEnd;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MovieDTO movieDTO = (MovieDTO) o;

        if (duration != movieDTO.duration) return false;
        if (rating != movieDTO.rating) return false;
        if (description != null ? !description.equals(movieDTO.description) : movieDTO.description != null)
            return false;
        if (genre != null ? !genre.equals(movieDTO.genre) : movieDTO.genre != null) return false;
        if (rentEnd != null ? !rentEnd.equals(movieDTO.rentEnd) : movieDTO.rentEnd != null) return false;
        if (rentStart != null ? !rentStart.equals(movieDTO.rentStart) : movieDTO.rentStart != null) return false;
        if (title != null ? !title.equals(movieDTO.title) : movieDTO.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + duration;
        result = 31 * result + (rentStart != null ? rentStart.hashCode() : 0);
        result = 31 * result + (rentEnd != null ? rentEnd.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + rating;
        return result;
    }
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM y");
    @Override
    public String toString() {
        return "MovieDTO{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", duration=" + duration +
                ", rentStart=" + rentStart.format(formatter) +
                ", rentEnd=" + rentEnd +
                ", genre='" + genre + '\'' +
                ", rating=" + rating +
                "} " + super.toString();
    }
}
