package dto;


import model.Hall;
import model.Movie;

import java.sql.Time;
import java.time.LocalTime;

/**
 * Created by Svetik on 12.06.2016.
 *
 */
public class SessionDTO extends Entity<Integer> {
    private LocalTime time;
    private double price;
    private Hall hall;
    private Movie movie;


    public SessionDTO() {
    }

    public SessionDTO(LocalTime time, double price, Hall hall, Movie movie) {
        this.time = time;
        this.price = price;
        this.hall = hall;
        this.movie = movie;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public String toString() {
        return "SessionDTO{" +
                "time='" + time + '\'' +
                ", price=" + price +
                '}';
    }
}
