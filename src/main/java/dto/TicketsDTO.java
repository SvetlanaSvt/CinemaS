package dto;


import model.Rows;
import model.Session;
import model.User;

/**
 * Created by Svetik on 19.06.2016.
 *
 */
public class TicketsDTO extends Entity<Integer> {
    private int rowNumber;
    private int seatNumber;
    private boolean sold;
    private User user;
    private Session session;
    private Rows rows;

    public TicketsDTO() {
    }

    public TicketsDTO(int rowNumber, int seatNumber, boolean sold, User user, Session session, Rows rows) {
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.sold = sold;
        this.user = user;
        this.session = session;
        this.rows = rows;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Rows getRows() {
        return rows;
    }

    public void setRows(Rows rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "TicketsDTO{" +
                "rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", sold=" + sold +
                ", user=" + user +
                ", session=" + session +
                ", rows=" + rows +
                '}';
    }
}
