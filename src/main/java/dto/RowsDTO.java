package dto;


import model.Hall;

/**
 * Created by Svetik on 18.06.2016.
 *
 */
public class RowsDTO extends Entity<Integer> {
    private int rowNumber;
    private int seatQuantity;
    private Hall hall;

    public RowsDTO() {
    }

    public RowsDTO(Hall hall, int rowNumber, int seatQuantity) {
        this.hall = hall;
        this.rowNumber = rowNumber;
        this.seatQuantity = seatQuantity;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatQuantity() {
        return seatQuantity;
    }

    public void setSeatQuantity(int seatQuantity) {
        this.seatQuantity = seatQuantity;
    }

    @Override
    public String toString() {
        return "RowsDTO{" +
                "rowNumber=" + rowNumber +
                ", seatQuantity=" + seatQuantity +
                '}';
    }
}
