package dto;



/**
 * Created by Svetik on 17.06.2016.
 *
 */
public class HallDTO extends Entity<Integer> {
    private String hallName;
    private int row;
    private int seat;

    public HallDTO() {
    }

    public HallDTO(String hallName, int row, int seat) {
        this.hallName = hallName;
        this.row = row;
        this.seat = seat;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    @Override
    public String toString() {
        return "HallDTO{" +
                "name='" + hallName + '\'' +
                '}';
    }
}
