package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.RowsDTO;
import mapper.BeanMapper;
import model.Rows;
import service.api.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Svetik on 18.06.2016.
 *
 */
public class RowsServiceImpl implements Service<Integer, RowsDTO> {

    private static RowsServiceImpl service;
    private Dao<Integer, Rows> rowsDao;
    private BeanMapper beanMapper;

    public RowsServiceImpl() {
        rowsDao = DaoFactory.getInstance().getRowsDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static synchronized RowsServiceImpl getInstance() {
        if(service == null) {
            service = new RowsServiceImpl();
        }
        return service;
    }

    @Override
    public List<RowsDTO> getAll() {
        List<Rows> rows = rowsDao.getAll();
        System.out.println(rows);
//        List<RowsDTO> rowsDTOs = beanMapper.listMapToList(rows, RowsDTO.class);
//        return rowsDTOs;
        return null;
    }

    @Override
    public RowsDTO getById(Integer id) {
        Rows rows = rowsDao.getById(id);
        RowsDTO rowsDTO = beanMapper.singleMapper(rows, RowsDTO.class);
        return rowsDTO;
    }

    public List<RowsDTO> getRowsByHallId(Integer id) {
        List<Rows> rowsList = rowsDao.getAll();
        List<Rows> rows = new ArrayList<>();
        for (Rows row : rowsList) {
            if(row.getHall().getId() == id) {
                rows.add(row);
            }
        }
        List<RowsDTO> rowsDTOs = beanMapper.listMapToList(rows, RowsDTO.class);
        return rowsDTOs;
    }

    @Override
    public void save(RowsDTO entity) {
        Rows rows = beanMapper.singleMapper(entity, Rows.class);
        rowsDao.save(rows);
    }

    @Override
    public void delete(Integer key) {
        rowsDao.delete(key);
    }

    @Override
    public void update(RowsDTO entity) {
//        NOP
    }
}
