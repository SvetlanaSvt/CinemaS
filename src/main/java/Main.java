import dao.DaoFactory;
import dao.impl.RoleDaoImpl;
import dto.SessionDTO;
import dto.UserDTO;
import model.Hall;
import model.Movie;
import model.Role;
import service.impl.*;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Created by Svetik on 17.06.2016.
 *
 */
public class Main {
    public static void main(String[] args) {
//        SessionDTO sessionDTO = new SessionDTO(Time.valueOf(LocalTime.of(12, 0)), 70.0, 1, 1);
//        SessionServiceImpl.getInstance().save(sessionDTO);
//        System.out.println(SessionServiceImpl.getInstance().getAll());
//        new Movie("В поисках Дори", "Любимые герои снова воссоединятся: Марлин, Немо и, конечно, голубая забывчивый Дори. Рыбка Дори пытается выяснить тайны своего прошлого, вспомнить своих родителей и в конце концов узнать, кто научил ее говорить по-китовому!", 103, LocalDate.of(2016, 06, 16), LocalDate.of(2016, 07, 06), "Мультфильм", 5)
//        List<UserDTO> userDTOs = UserServiceImpl.getInstance().getAll();
//        System.out.println(userDTOs);
//        System.out.println(UserServiceImpl.getInstance().getByLogin("Svetik"));
//        Role role = DaoFactory.getInstance().getRoleDao().getBy("role_name", "User");
//        UserDTO userDTO = new UserDTO("MoviePro", "111111", "", "", "aa11111@mail.ru", "Мужской", LocalDate.of(1998, 5, 16), role);
//        UserServiceImpl.getInstance().save(userDTO);
//        System.out.println(SessionServiceImpl.getInstance().getAll());
//        System.out.println(UserServiceImpl.getInstance().getAll());
//        System.out.println(RowsServiceImpl.getInstance().getById(1));
//        System.out.println(RowsServiceImpl.getInstance().getRowsByHallId(1));
//        System.out.println(MovieServiceImpl.getInstance().getAll());
//        System.out.println(RoleServiceImpl.getInstance().getAll());
//        System.out.println(HallServiceImpl.getInstance().getAll());
//        System.out.println(DaoFactory.getInstance().getRowsDao().getAll());
//        System.out.println(DaoFactory.getInstance().getRowsDao().getBy("row_number", "1"));
        System.out.println(DaoFactory.getInstance().getRowsDao().getListBy("hall_id", "1"));

    }
}
