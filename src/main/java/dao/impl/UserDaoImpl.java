package dao.impl;

import dao.DaoFactory;
import model.User;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.UPDATE_USER;
import static dao.SQLs.INSERT_USER;

/**
 * Created by Svetik on 18.06.2016.
 *
 */
public final class UserDaoImpl extends CrudDAO<User> {
    public UserDaoImpl() {
        super(User.class);
    }

    private void setStatement(User entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getLogin());
        preparedStatement.setString(2, entity.getPassword());
        preparedStatement.setString(3, entity.getFirstName());
        preparedStatement.setString(4, entity.getLastName());
        preparedStatement.setString(5, entity.getEmail());
        preparedStatement.setString(6, entity.getSex());
        preparedStatement.setDate(7, Date.valueOf(entity.getBirthday()));
        preparedStatement.setInt(8, entity.getRole().getId());
    }


    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> result = new LinkedList<>();
        while (resultSet.next()) {
            User users = new User();
            users.setId(resultSet.getInt("id"));
            users.setLogin(resultSet.getString("login"));
            users.setPassword(resultSet.getString("password"));
            users.setFirstName(resultSet.getString("first_name"));
            users.setLastName(resultSet.getString("last_name"));
            users.setEmail(resultSet.getString("email"));
            users.setSex(resultSet.getString("sex"));
            users.setBirthday(resultSet.getDate("birthday").toLocalDate());
            users.setRole(DaoFactory.getInstance().getRoleDao().getById(resultSet.getInt("role_id")));
            result.add(users);
        }
        return result;
    }


}
