package dao.impl;

import dao.DaoFactory;
import model.Tickets;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_TICKETS;
import static dao.SQLs.UPDATE_TICKETS;

/**
 * Created by Svetik on 19.06.2016.
 *
 */
public final class TicketsDaoImpl extends CrudDAO<Tickets> {

    public TicketsDaoImpl() {
        super(Tickets.class);
    }

    private void setStatement(Tickets entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, entity.getRowNumber());
        preparedStatement.setInt(2, entity.getSeatNumber());
        preparedStatement.setBoolean(3, entity.isSold());
        preparedStatement.setInt(4, entity.getUser().getId());
        preparedStatement.setInt(5, entity.getSession().getId());
        preparedStatement.setInt(6, entity.getRows().getId());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Tickets entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TICKETS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Tickets entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TICKETS, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<Tickets> readAll(ResultSet resultSet) throws SQLException {
        List<Tickets> result = new LinkedList<>();
        while (resultSet.next()) {
            Tickets tickets = new Tickets();
            tickets.setId(resultSet.getInt("id"));
            tickets.setRowNumber(resultSet.getInt("row_number"));
            tickets.setSeatNumber(resultSet.getInt("seat_number"));
            tickets.setSold(resultSet.getBoolean("is_sold"));
            tickets.setUser(DaoFactory.getInstance().getUsersDao().getById(resultSet.getInt("user_id")));
            tickets.setSession(DaoFactory.getInstance().getSessionDao().getById(resultSet.getInt("session_id")));
            tickets.setRows(DaoFactory.getInstance().getRowsDao().getById(resultSet.getInt("rows_id")));
            result.add(tickets);
        }
        return result;
    }
}
