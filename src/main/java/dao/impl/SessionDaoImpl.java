package dao.impl;

import dao.DaoFactory;
import model.Session;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static dao.SQLs.INSERT_SESSION;
import static dao.SQLs.UPDATE_SESSION;

/**
 * Created by Svetik on 12.06.2016.
 *
 */
public final class SessionDaoImpl extends CrudDAO<Session> {

    public SessionDaoImpl() {
        super(Session.class);
    }

    private void setStatement(Session entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setTime(1, Time.valueOf(entity.getTime()));
        preparedStatement.setDouble(2, entity.getPrice());
        preparedStatement.setInt(3, entity.getHall().getId());
        preparedStatement.setInt(4, entity.getMovie().getId());
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SESSION);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SESSION, Statement.RETURN_GENERATED_KEYS);
        setStatement(entity, preparedStatement);
        return preparedStatement;
    }

    @Override
    protected List<Session> readAll(ResultSet resultSet) throws SQLException {
        List<Session> result = new LinkedList<>();
        while (resultSet.next()) {
            Session session = new Session();
            session.setId(resultSet.getInt("id"));
            session.setTime(resultSet.getTime("time").toLocalTime());
            session.setPrice(resultSet.getDouble("price"));
            session.setHall(DaoFactory.getInstance().getHallDao().getById(resultSet.getInt("hall_id")));
            session.setMovie(DaoFactory.getInstance().getMovieDao().getById(resultSet.getInt("movie_id")));
            result.add(session);
        }
        return result;
    }


}
