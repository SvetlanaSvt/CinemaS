package dao.impl;

import model.Role;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Svetik on 18.06.2016.
 *
 */
public final class RoleDaoImpl extends CrudDAO<Role>{

    public RoleDaoImpl() {
        super(Role.class);
    }

//    private void setStatement(Role entity, PreparedStatement preparedStatement) throws SQLException {
//        preparedStatement.setString(1, entity.getRoleName());
//    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Role entity) throws SQLException {
        return null;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Role entity) throws SQLException {
        return null;
    }

    @Override
    protected List<Role> readAll(ResultSet resultSet) throws SQLException {
        List<Role> result = new LinkedList<>();
        while (resultSet.next()) {
            Role role = new Role();
            role.setId(resultSet.getInt("id"));
            role.setRoleName(resultSet.getString("role_name"));
            result.add(role);
        }
        return result;
    }
}
