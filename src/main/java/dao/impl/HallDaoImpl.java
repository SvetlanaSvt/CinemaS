package dao.impl;

import model.Hall;
import model.Movie;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Svetik on 17.06.2016.
 *
 */
public final class HallDaoImpl extends CrudDAO<Hall> {

    public HallDaoImpl() {
        super(Hall.class);
    }

    private void setStatement(Hall entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getHallName());
        preparedStatement.setInt(2, entity.getRow());
        preparedStatement.setInt(1, entity.getSeat());
    }
    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall entity) throws SQLException {
        return null;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall entity) throws SQLException {
        return null;
    }

    @Override
    protected List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> result = new LinkedList<>();
        while (resultSet.next()) {
            Hall hall = new Hall();
            hall.setId(resultSet.getInt("id"));
            hall.setHallName(resultSet.getString("hall_name"));
            hall.setRow(resultSet.getInt("row"));
            hall.setSeat(resultSet.getInt("seat"));
            result.add(hall);
        }
        return result;
    }
}
