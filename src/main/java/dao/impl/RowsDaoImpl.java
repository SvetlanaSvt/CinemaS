package dao.impl;

import dao.DaoFactory;
import model.Rows;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Svetik on 18.06.2016.
 *
 */
public final class RowsDaoImpl extends CrudDAO<Rows> {

    public RowsDaoImpl() {
        super(Rows.class);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Rows entity) throws SQLException {
        return null;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Rows entity) throws SQLException {
        return null;
    }

    @Override
    protected List<Rows> readAll(ResultSet resultSet) throws SQLException {
        List<Rows> result = new LinkedList<>();
        while (resultSet.next()) {
            Rows rows = new Rows();
            rows.setId(resultSet.getInt("id"));
            rows.setRowNumber(resultSet.getInt("row_number"));
            rows.setSeatQuantity(resultSet.getInt("seat_quantity"));
            rows.setHall(DaoFactory.getInstance().getHallDao().getById(resultSet.getInt("hall_id")));
            result.add(rows);
        }
        return result;
    }
}
