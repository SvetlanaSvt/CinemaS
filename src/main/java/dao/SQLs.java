package dao;

/**
 * Created by Svetik on 08.06.2016.
 *
 */
public class SQLs {
    public static final String SELECT_ALL = "Select * from %s";
    public static final String FIND_BY_ID = "Select * from %s where id = ?";
    public static final String FIND_BY = "Select * from %s where %s = ?";
    public static final String DELETE_BY_ID = "DELETE FROM %s WHERE id = ?";
    public static final String INSERT_MOVIE = "Insert into movie (title, description, duration, rent_start, rent_end, genre, rating) values (?,?,?,?,?,?,?)";
    public static final String UPDATE_MOVIE = "UPDATE movie SET title = ?, description = ?, duration = ?, rent_start = ?, rent_end = ?, genre = ?, rating = ?, WHERE id = ?";
    public static final String UPDATE_SESSION = "UPDATE session SET time = ?, price = ?, hall_id = ?, movie_id = ?, WHERE id = ?";
    public static final String INSERT_SESSION = "INSERT INTO session (time, price, hall_id, movie_id) VALUES (?,?,?,?)";
    public static  final String UPDATE_USER = "UPDATE user SET login = ?, password = ?, first_name = ?, last_name = ?, email = ?, sex = ?, birthday = ?, role_id = ?, WHERE id = ?";
    public static  final String INSERT_USER = "Insert into user (login, password, first_name, last_name, email, sex, birthday, role_id) values (?,?,?,?,?,?,?,?)";
    public static  final String UPDATE_TICKETS = "UPDATE tickets SET row_number = ?, seat_number = ?, is_sold = ?, user_id = ?, session_id = ?, rows_id = ? WHERE id = ?";
    public static  final String INSERT_TICKETS = "Insert into tickets (row_number, seat_number, is_sold, user_id, session_id, rows_id) values (?,?,?,?,?,?)";

}
