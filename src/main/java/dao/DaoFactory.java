package dao;

import dao.api.Dao;
import dao.impl.*;
import helpers.PropertyHolder;
import model.*;

/**
 * Created by Svetik on 08.06.2016.
 *
 */
public class DaoFactory {
    private static DaoFactory instance = null;

    private Dao<Integer, Movie> movieDao;
    private Dao<Integer, Session> sessionDao;
    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Rows> rowsDao;
    private Dao<Integer, User> usersDao;
    private Dao<Integer, Tickets> ticketsDao;

    private DaoFactory(){
        loadDaos();
    }

    public static DaoFactory getInstance(){
        if(instance == null){
            instance = new DaoFactory();
        }
        return instance;
    }

    private void loadDaos() {
        if(PropertyHolder.getInstance().isInMemoryDB()){

        }else{
            movieDao = new MovieDaoImpl();
            sessionDao = new SessionDaoImpl();
            hallDao = new HallDaoImpl();
            roleDao = new RoleDaoImpl();
            rowsDao = new RowsDaoImpl();
            usersDao = new UserDaoImpl();
            ticketsDao = new TicketsDaoImpl();
        }
    }

    public Dao<Integer, Movie> getMovieDao() {
        return movieDao;
    }

    public void setMovieDao(Dao<Integer, Movie> movieDao) {
        this.movieDao = movieDao;
    }

    public Dao<Integer, Session> getSessionDao() {
        return sessionDao;
    }

    public void setSessionDao(Dao<Integer, Session> sessionDao) {
        this.sessionDao = sessionDao;
    }

    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }

    public void setHallDao(Dao<Integer, Hall> hallDao) {
        this.hallDao = hallDao;
    }

    public Dao<Integer, Role> getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(Dao<Integer, Role> roleDao) {
        this.roleDao = roleDao;
    }

    public Dao<Integer, Rows> getRowsDao() {
        return rowsDao;
    }

    public void setRowsDao(Dao<Integer, Rows> rowsDao) {
        this.rowsDao = rowsDao;
    }

    public Dao<Integer, User> getUsersDao() {
        return usersDao;
    }

    public void setUsersDao(Dao<Integer, User> usersDao) {
        this.usersDao = usersDao;
    }

    public Dao<Integer, Tickets> getTicketsDao() {
        return ticketsDao;
    }

    public void setTicketsDao(Dao<Integer, Tickets> ticketsDao) {
        this.ticketsDao = ticketsDao;
    }
}
