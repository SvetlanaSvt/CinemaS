package model;

/**
 * Created by Svetik on 10.06.2016.
 *
 */
public class Tickets extends Entity<Integer> {
    private int rowNumber;
    private int seatNumber;
    private boolean sold;
    private User user;
    private Session session;
    private Rows rows;

    public Tickets() {
    }

    public Tickets(int rowNumber, int seatNumber, boolean sold, User user, Session session, Rows rows) {
        this.rowNumber = rowNumber;
        this.seatNumber = seatNumber;
        this.sold = sold;
        this.user = user;
        this.session = session;
        this.rows = rows;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Rows getRows() {
        return rows;
    }

    public void setRows(Rows rows) {
        this.rows = rows;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Tickets tickets = (Tickets) o;

        if (rowNumber != tickets.rowNumber) return false;
        if (seatNumber != tickets.seatNumber) return false;
        if (sold != tickets.sold) return false;
        if (rows != null ? !rows.equals(tickets.rows) : tickets.rows != null) return false;
        if (session != null ? !session.equals(tickets.session) : tickets.session != null) return false;
        if (user != null ? !user.equals(tickets.user) : tickets.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + rowNumber;
        result = 31 * result + seatNumber;
        result = 31 * result + (sold ? 1 : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (rows != null ? rows.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tickets{" +
                "rowNumber=" + rowNumber +
                ", seatNumber=" + seatNumber +
                ", sold=" + sold +
                ", user=" + user +
                ", session=" + session +
                ", rows=" + rows +
                '}';
    }
}
