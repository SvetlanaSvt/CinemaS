package model;

/**
 * Created by Svetik on 10.06.2016.
 *
 */
public class Rows extends Entity<Integer> {
    private int rowNumber;
    private int seatQuantity;
    private Hall hall;

    public Rows() {
    }

    public Rows(Hall hall, int rowNumber, int seatQuantity) {
        this.hall = hall;
        this.rowNumber = rowNumber;
        this.seatQuantity = seatQuantity;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getSeatQuantity() {
        return seatQuantity;
    }

    public void setSeatQuantity(int seatQuantity) {
        this.seatQuantity = seatQuantity;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Rows rows = (Rows) o;

        if (rowNumber != rows.rowNumber) return false;
        if (seatQuantity != rows.seatQuantity) return false;
        if (hall != null ? !hall.equals(rows.hall) : rows.hall != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + rowNumber;
        result = 31 * result + seatQuantity;
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rows{" +
                "rowNumber=" + rowNumber +
                ", seatQuantity=" + seatQuantity +
                ", hall=" + hall +
                '}';
    }
}
