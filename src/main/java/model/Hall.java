package model;

/**
 * Created by Svetik on 10.06.2016.
 *
 */
public class Hall extends Entity<Integer> {
    private String hallName;
    private int row;
    private int seat;

    public Hall() {
    }

    public Hall(String hallName, int row, int seat) {
        this.hallName = hallName;
        this.row = row;
        this.seat = seat;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Hall hall = (Hall) o;

        if (row != hall.row) return false;
        if (seat != hall.seat) return false;
        if (hallName != null ? !hallName.equals(hall.hallName) : hall.hallName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (hallName != null ? hallName.hashCode() : 0);
        result = 31 * result + row;
        result = 31 * result + seat;
        return result;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "hallName='" + hallName + '\'' +
                '}';
    }
}
