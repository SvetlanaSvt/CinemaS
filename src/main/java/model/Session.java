package model;


import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

/**
 * Created by Svetik on 10.06.2016.
 *
 */
public class Session extends Entity<Integer> {
    private LocalTime time;
    private double price;
    private Hall hall;
    private Movie movie;



    public Session() {
    }

    public Session(LocalTime time, double price, Hall hall, Movie movie) {
        this.time = time;
        this.price = price;
        this.hall = hall;
        this.movie = movie;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Session session = (Session) o;

        if (Double.compare(session.price, price) != 0) return false;
        if (hall != null ? !hall.equals(session.hall) : session.hall != null) return false;
        if (movie != null ? !movie.equals(session.movie) : session.movie != null) return false;
        if (time != null ? !time.equals(session.time) : session.time != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + (time != null ? time.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        result = 31 * result + (movie != null ? movie.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Сеанс: " + time + ". Цена билета: " + price;
    }

}
