<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 25.06.2016
  Time: 23:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}">
  <jsp:include page="../common/header.jsp"/>
</c:if>
<c:if test="${user.login ne null}">
  <jsp:include page="../common/header-reg.jsp"/>
</c:if>
<html>
<head>
  <title>Бронь билетов</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/lenta.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
  <%--<script>--%>
  <%--function agreeForm(f) {--%>
  <%--// Если поставлен флажок, снимаем блокирование кнопки--%>
  <%--if (f.agree.checked) f.submit.disabled = 0--%>
  <%--// В противном случае вновь блокируем кнопку--%>
  <%--else f.submit.disabled = 1--%>
  <%--}--%>
  <%--</script>--%>
  <%--<script>--%>
  <%--function textMsg(msg) {--%>
  <%--document.getElementById('text').innerHTML = msg;--%>
  <%--document.getElementById('msg').style.display = 'block';--%>
  <%--}--%>
  <%--function closeMsg() {--%>
  <%--document.getElementById('msg').style.display = 'none';--%>
  <%--}--%>
  <%--</script>--%>
</head>
<body>
<div class="hall">
  <div><b>${movie.title}. Начало сеанса ${sessionDTO.time}</b></div>
  <div><h3>${hall.hallName} зал</h3></div>
  <div class="hall_display">
    Экран
  </div>
</div>
<br/>
<%--enabled--%>
<form class="hall" name="bookingForm" action="${pageContext.servletContext.contextPath}/pages/user/book-tickets">
  <div class="hall_row">
    <input type="hidden" name="sessionDTO" value="${sessionDTO.id}">
    <input type="hidden" name="movie" value="${movie.id}">
    <input type="hidden" name="hall" value="${hall.id}">
    <input type="hidden" name="rowNumbers" value="${rowNumbers}">
    <%--<input type="hidden" name="rowsList" value="${rowsList}">--%>

    <div>
      <c:forEach items="${rowNumbers}" var="rowNumber">
        <div>${rowNumber} ряд <c:forEach items="${rowsList}" var="rows">
          <c:if test="${rows.rowNumber eq rowNumber}">
            <input type="checkbox" class="seat" name="${rows.rowNumber}${rows.seatQuantity}"
                   id="${rows.rowNumber}${rows.seatQuantity}" value="${rows.rowNumber}${rows.seatQuantity}" title="${rows.seatQuantity} место"
                   <c:forEach items="${ticketsList}" var="tickets"><c:if test="${tickets.rows.id eq rows.id}">disabled</c:if></c:forEach>>
          </c:if>
        </c:forEach> ${rowNumber} ряд
        </div>
      </c:forEach>
    </div>
    <input type="submit" value="Забронировать" name="booking">
  </div>
</form>
<div>

</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>