<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 05.07.2016
  Time: 15:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../common/header-reg.jsp"/>
<html>
<head>
  <title>Личный кабинет</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/lenta.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">

</head>
<body>
<div class="ticket">
  <div><h3>Уважаемый ${user.login}!</h3></div>
  <div><b>Вы бронировали билеты:</b></div>
    <c:forEach items="${ticketsList}" var="ticket">
      <div><i>"${ticket.session.movie.title}". ${ticket.session}. ${ticket.rowNumber} ряд ${ticket.seatNumber} место</i></div>

    </c:forEach>
  </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>
