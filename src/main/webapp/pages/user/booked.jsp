<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 29.06.2016
  Time: 15:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}">
  <jsp:include page="../common/header.jsp"/>
</c:if>
<c:if test="${user.login ne null}">
  <jsp:include page="../common/header-reg.jsp"/>
</c:if>
<html>
<head>
  <title>Забронированно</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/lenta.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="ticket">
  <div><h3>Уважаемый ${user.login}!</h3></div>
  <div><p>Вы забронировали билеты на фильм "${movie.title}"</p></div>
  <div><p> Начало сеанса: ${sessionDTO.time}. Цена билета ${sessionDTO.price}</p></div>
  <div><p>Забронировано:</p>
    <c:forEach items="${rowsListBooked}" var="ticket">
      <div><p>${ticket.rowNumber} ряд ${ticket.seatQuantity} место</p></div>
    </c:forEach>
  </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>
