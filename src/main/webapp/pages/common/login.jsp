<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="header-reg.jsp"/></c:if>
<html>
<head>
    <title>Логин</title>
    <style>
        body {
            margin: 0;
            background-image: url(${pageContext.servletContext.contextPath}/pages/images/log.jpg);
            background-repeat: no-repeat;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="login_form">
    <div align="center">
        <div class="login_form_c"><h3><c:out value="${sessionScope.message}"/></h3></div>
        <form name="loginForm" method="post" action="${pageContext.servletContext.contextPath}/login">
            <input type="hidden" name="sessionId" value="${sessionId}">
            <table>
                <tr>
                    <td><p>Логин:</p></td>
                    <td><input type="text" name="login"></td>
                </tr>
                <tr>
                    <td><p>Пароль:</p></td>
                    <td><input type="password" name="password"></td>
                    <td><input type="submit" value="Вход"/></td>
                </tr>
            </table>
        </form>
    </div>
</div>
</body>
</html>
