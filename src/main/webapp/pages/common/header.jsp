<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <style>
    <%@include file='../css/style.css'%>
  </style>
    <title>Кинотеатр</title>
</head>
<body>
<div>
  <div class="header_container">
    <div class="login_menu">
      <div align="center">
        <a href="${pageContext.servletContext.contextPath}/pages/common/registration.jsp"><b>Регистрация</b></a><br/>
        <br/>

        <a href="${pageContext.servletContext.contextPath}/pages/common/loginEnter"><b>Вход</b></a><br/>
        <br/>
        <a href="${pageContext.servletContext.contextPath}/"><b>На главную</b></a><br/>
      </div>
    </div>
    <div align="center">
      <br/>
      <br/>
      <h1>Кинотеатр "КиноПрофи"</h1>
    </div>
  </div>
</div>
</body>
</html>
