<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <style>
    <%@include file='../css/style.css'%>

  </style>
  <title>Кинотеатр</title>
</head>
<body>
<div>
  <div class="header_container">
    <div class="login_menu">
      <div align="center">
        <a href="${pageContext.servletContext.contextPath}/pages/user/cabinet"><b>Личный кабинет</b></a><br/>
        <br/>
        <b><c:out value="${user.login}"/></b>
        <%--<c:out value="${user.login}"/>--%>
        <a href="${pageContext.servletContext.contextPath}/exit"><b>Выход</b></a><br/>
        <br/>
        <a href="${pageContext.servletContext.contextPath}/main"><b>На главную</b></a>
      </div>
    </div>
    <div align="center">
      <br/>
      <br/>
      <h1>Кинотеатр "КиноПрофи"</h1>
    </div>
  </div>
</div>
</body>
</html>
