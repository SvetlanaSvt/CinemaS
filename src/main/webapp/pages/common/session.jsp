<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="header-reg.jsp"/></c:if>
<html>
<head>
    <title>Session</title>
    <style>
        body{
            margin: 0;
            background-image: url(${pageContext.servletContext.contextPath}/pages/images/lenta.jpg);
            background-repeat: no-repeat;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="movie" align="center">
    <%--<form action="/login>"><input type="hidden" name="id" value="${movieDTOs.id}"/></form>--%>
    <div class="show_info">
        <i>Название: </i>${movieDTOs.title}
    </div>
    <div class="show_info">
        <i>Жанр: </i> ${movieDTOs.genre}
    </div>
    <div class="show_info">
        <i>Описание: </i>${movieDTOs.description}
    </div>
    <div class="show_info">
        <i>Рейтинг: </i>${movieDTOs.rating}
    </div>
    <div class="show_info">
        <i>Дата начала проката: </i>${movieDTOs.rentStart}
    </div>
    <div class="show_info">
        <i>Дата окончания проката: </i>${movieDTOs.rentEnd}
    </div>
    <div title="Забронировать билет" class="show_info">
        <c:forEach items="${sessionDTOs}" var="session">
            <p>Сеанс: <a href="${pageContext.servletContext.contextPath}pages/user/session?id=${session.id}">${session.time}</a> Стоимость билета: ${session.price} грн.</p>
        </c:forEach>
    </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="footer.jsp"/>