<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="header-reg.jsp"/></c:if>
<html>
<head>
    <title>Kинотеатр</title>
    <style>
        body {
            margin: 0;
            background-image: url(${pageContext.servletContext.contextPath}/pages/images/lenta.jpg);
            background-repeat: no-repeat;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="movie">
    <h1>Список фильмов, демонстрирующихся в кинотеатре</h1>

    <div class="movie_content">
        <c:forEach items="${movieDTOList}" var="movies">
            <h3><a href="${pageContext.servletContext.contextPath}/movie?id=${movies.id}">${movies.title}</a><i>${movies.genre}</i></h3>
        </c:forEach>
    </div>
    <div>
        <c:forEach items="${localDates}" var="date">
            <i>${date}</i>
        </c:forEach>
    </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="footer.jsp"/>