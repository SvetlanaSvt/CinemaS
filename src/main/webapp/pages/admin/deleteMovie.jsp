<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 05.07.2016
  Time: 20:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="../common/header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="../common/header-reg.jsp"/></c:if>
<html>
<head>
  <title>Удаление фильма</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/reg.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="movie">
  <c:out value="${sessionScope.message}"/>
  <h3>Внимание! Удалив фильм, вы также удалите всю информацию,  связанную с этим фильмом!</h3>
  <h3>Выберите фильм, который хотите удалить</h3>
  <div class="movie_content">
    <c:forEach items="${movieDTOList}" var="movies">
      <h4><a href="${pageContext.servletContext.contextPath}/pages/admin/deleteMovieId?id=${movies.id}">${movies.title}</a></h4>
    </c:forEach>
  </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>