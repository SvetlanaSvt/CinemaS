<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 23.06.2016
  Time: 22:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="../common/header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="../common/header-reg.jsp"/></c:if>
<html>
<head>
    <title>Редактирование фильмов</title>
    <style>
        body{
            margin: 0;
            background-image: url(${pageContext.servletContext.contextPath}/pages/images/reg.jpg);
            background-repeat: no-repeat;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="update_form">
    <div align="center">
        <form name="updateFilm" method="post" action="${pageContext.servletContext.contextPath}/pages/admin/update">
            <table>
                <tr>
                    <td>Название фильма:</td>
                    <td><input type="text" name="title" required class="inPut" maxlength="40"></td>
                </tr>
                <tr>
                    <td>Описание:</td>
                    <td><input type="text" name="description" required class="inPut" maxlength="400"></td>
                </tr>
                <tr>
                    <td>Продолжительность (в минутах):</td>
                    <td><input type="text" name="duration" required class="inPut"></td>
                </tr>
                <tr>
                    <td>Дата начала показа:</td>
                    <td>
                        <tt><select type="text" name="dayStart" required class="inPutDate">
                            <option value selected>День</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select></tt>
                        <tt><select type="text" name="monthStart" required class="inPutDate">
                            <option value selected>Месяц</option>
                            <option value="1">Январь</option>
                            <option value="2">Февраль</option>
                            <option value="3">Март</option>
                            <option value="4">Апрель</option>
                            <option value="5">Май</option>
                            <option value="6">Июнь</option>
                            <option value="7">Июль</option>
                            <option value="8">Август</option>
                            <option value="9">Сентябрь</option>
                            <option value="10">Октябрь</option>
                            <option value="11">Ноябрь</option>
                            <option value="12">Декабрь</option>
                        </select></tt>
                        <tt><select type="text" name="yearStart" required class="inPutDate">
                            <option value selected>Год</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                        </select></tt>
                        </td>
                </tr>
                <tr>
                    <td>Дата окончания показа:</td>
                    <td>
                        <tt><select type="text" name="dayEnd" required class="inPutDate">
                            <option value selected>День</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select></tt>
                        <tt><select type="text" name="monthEnd" required class="inPutDate">
                            <option value selected>Месяц</option>
                            <option value="1">Январь</option>
                            <option value="2">Февраль</option>
                            <option value="3">Март</option>
                            <option value="4">Апрель</option>
                            <option value="5">Май</option>
                            <option value="6">Июнь</option>
                            <option value="7">Июль</option>
                            <option value="8">Август</option>
                            <option value="9">Сентябрь</option>
                            <option value="10">Октябрь</option>
                            <option value="11">Ноябрь</option>
                            <option value="12">Декабрь</option>
                        </select></tt>
                        <tt><select type="text" name="yearEnd" required class="inPutDate">
                            <option value selected>Год</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                        </select></tt>
                        </td>
                </tr>

                <tr>
                    <td>Жанр:</td>
                    <td><select type="text" name="genre" required class="inPut">
                        <option value selected>Жанр</option>
                        <option value="Боевик">Боевик</option>
                        <option value="Вестерн">Вестерн</option>
                        <option value="Детектив">Детектив</option>
                        <option value="Документальный">Документальный</option>
                        <option value="Драма">Драма</option>
                        <option value="Исторический">Исторический</option>
                        <option value="Комедия">Комедия</option>
                        <option value="Криминал">Криминал</option>
                        <option value="Мелодрама">Мелодрама</option>
                        <option value="Мистика">Мистика</option>
                        <option value="Мультфильм">Мультфильм</option>
                        <option value="Мюзикл">Мюзикл</option>
                        <option value="Приключения">Приключения</option>
                        <option value="Семейный">Семейный</option>
                        <option value="Триллер">Триллер</option>
                        <option value="Ужасы">Ужасы</option>
                        <option value="Фантастика">Фантастика</option>
                        <option value="Фэнтези">Фэнтези</option>
                        <option value="Эротика">Эротика</option>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>Рейтинг:</td>
                    <td><input type="text" name="rating" required class="inPut"></td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><input type="submit" value="Сохранить" name="reg"></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>