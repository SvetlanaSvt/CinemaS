<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 07.07.2016
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="../common/header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="../common/header-reg.jsp"/></c:if>
<html>
<head>
  <title>Удаление сеансов</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/reg.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="movie">
  <c:out value="${sessionScope.message}"/>
  <h3>Внимание! Удалив сеанс, вы также удалите всю информацию,  связанную с этим сеансом!</h3>
  <h3>Выберите сеанс для удаления</h3>

  <div title="Удаление сеанса" class="movie_content">
    <c:forEach items="${sessionDTOs}" var="session">
      <p>Сеанс: <a href="${pageContext.servletContext.contextPath}/pages/admin/deleteSession?id=${session.id}">${session.time}</a></p>
    </c:forEach>
  </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>