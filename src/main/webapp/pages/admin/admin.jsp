<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 25.06.2016
  Time: 19:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="../common/header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="../common/header-reg.jsp"/></c:if>
<html>
<head>
  <title>Редактирование фильмов</title>
  <style>
    body{
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/reg.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="movie">
  <c:out value="${sessionScope.message}"/>
  <h3><a href="${pageContext.servletContext.contextPath}/pages/admin/updateMovie.jsp">Добавить фильм</a></h3>
  <h3><a href="${pageContext.servletContext.contextPath}/pages/admin/deleteMovie">Удалить фильм</a></h3>
  <h3><a href="${pageContext.servletContext.contextPath}/pages/admin/updateSession">Добавить сеансы</a></h3>
  <h3><a href="${pageContext.servletContext.contextPath}/pages/admin/deleteSessionMovieList">Удалить сеанс</a></h3>
  <h3><a href="${pageContext.servletContext.contextPath}/pages/admin/updateTicketsListMovie">Снять бронь билетов</a></h3>

<%--<h3><a href="${pageContext.servletContext.contextPath}/pages/admin/changeSession.jsp">Изменить сеансы</a></h3>--%>
  <%--<h3><a href="${pageContext.servletContext.contextPath}/pages/admin/changeMovie.jsp"></a>Редактировать фильм</h3>--%>

</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>