<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 25.06.2016
  Time: 19:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="../common/header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="../common/header-reg.jsp"/></c:if>
<html>
<head>
  <title>Добавление сеансов</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/reg.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="movie">
  <h1>${movieDTOs.title}</h1>
</div>
  <div class="movie_content">
      <%--<h3><a href="${pageContext.servletContext.contextPath}/pages/admin/sessionForm.jsp?id=${movies.id}">${movies.title}</a></h3>--%>
  </div>
  <div class="update_form">
    <div align="center">
      <form name="updateFilm" method="post" action="${pageContext.servletContext.contextPath}/pages/admin/saveSession">
        <table>
          <tr>
            <%--<td>${movieDTOs.title}:</td>--%>
            <td><input type="hidden" name="movieId" value="${movieDTOs.id}" required>
              <%--<option value="${movieDTOs.id}">${movieDTOs.id}</option>--%>
            <%--</select>--%>
            </td>
          </tr>
          <tr>
            <td>Время сеанса:</td>
            <td><input type="time" name="time" required class="inPut"></td>
          </tr>
          <tr>
            <td>Цена билета:</td>
            <td><input type="text" name="price" required class="inPut"></td>
          </tr>

          <tr>
            <td>Выберите зал:</td>
            <td><select type="text" name="hallName" required class="inPut">
              <option value selected>Зал</option>
              <option value="Синий">Синий</option>
              <option value="Зелёный">Зелёный</option>
              <option value="Красный">Красный</option>
            </select>
            </td>
          </tr>
          <tr>
            <td align="right" colspan="2"><input type="submit" value="Сохранить" name="reg"></td>
          </tr>
        </table>
      </form>
    </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>