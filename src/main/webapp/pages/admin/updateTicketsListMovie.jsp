<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 29.06.2016
  Time: 23:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="../common/header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="../common/header-reg.jsp"/></c:if>
<html>
<head>
  <title>Добавление сеансов</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/reg.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div class="movie">
  <c:out value="${sessionScope.message}"/>
  <h1>Выберите фильм для снятия брони билетов</h1>

  <div class="movie_content">
    <c:forEach items="${movieDTOList}" var="movies">
      <h3><a href="${pageContext.servletContext.contextPath}/pages/admin/updateTicketsListSession?movieId=${movies.id}">${movies.title}</a></h3>
    </c:forEach>
  </div>
</div>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>