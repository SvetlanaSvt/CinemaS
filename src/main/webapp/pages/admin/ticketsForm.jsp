<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 30.06.2016
  Time: 1:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${user.login eq null}"><jsp:include page="../common/header.jsp"/></c:if>
<c:if test="${user.login ne null}"><jsp:include page="../common/header-reg.jsp"/></c:if>
<html>
<head>
  <title>Форма для снятия брони</title>
  <style>
    body {
      margin: 0;
      background-image: url(${pageContext.servletContext.contextPath}/pages/images/reg.jpg);
      background-repeat: no-repeat;
    }
  </style>
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all">
</head>
<body>
<div style="position: absolute; bottom: auto">
  <div class="login_form_c"><h3><c:out value="${sessionScope.message}"/></h3></div>

  <c:forEach items="${ticketsList}" var="tickets">
    <c:if test="${tickets ne null}">
      <div style="color: #ffffff">Забронированно: ${tickets.rowNumber} ряд ${tickets.seatNumber} место</div>
    </c:if>
  </c:forEach>
</div>
<div class="hall">
  <div><b>${movie.title}. Начало сеанса ${sessionDTO.time}</b></div>
  <div><h3>${hall.hallName} зал</h3></div>
  <div class="hall_display">
    Экран
  </div>
</div>
<br/>
<form class="hall" name="bookingForm" action="${pageContext.servletContext.contextPath}/pages/admin/unBookTickets">
  <div class="hall_row">
    <input type="hidden" name="sessionDTO" value="${sessionDTO.id}">
    <input type="hidden" name="movie" value="${movie.id}">
    <input type="hidden" name="hall" value="${hall.id}">

    <div>
      <c:forEach items="${rowNumbers}" var="rowNumber">
        <div>${rowNumber} ряд <c:forEach items="${rowsList}" var="rows">
          <c:if test="${rows.rowNumber eq rowNumber}">
            <input type="checkbox" class="seat" name="${rows.rowNumber}${rows.seatQuantity}"
                   value="${rows.rowNumber}${rows.seatQuantity}" title="${rows.seatQuantity} место"
              <%--не работает if написанный ниже--%>
              <%--условие на == работает, на != нет, подскажите как исправить--%>
                  <c:forEach items="${ticketsList}" var="tickets"><c:if test="${rows.id ne tickets.rows.id}">disabled</c:if></c:forEach>
                    >
          </c:if>
        </c:forEach> ${rowNumber} ряд
        </div>
      </c:forEach>
    </div>
    <input type="submit" value="Снять бронь" name="booking">
  </div>
</form>
<div class="footer"></div>
</body>
</html>
<jsp:include page="../common/footer.jsp"/>